/*
Run this script on:

        localhost.QLDiem_TrienKhai    -  This database will be modified

to synchronize it with:

        localhost.QLDiem

You are recommended to back up your database before running this script

Script created by SQL Compare version 11.1.0 from Red Gate Software Ltd at 10/21/2017 2:56:05 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BANGTHONGKE]'
GO
CREATE TABLE [dbo].[BANGTHONGKE]
(
[HocKyID] [bigint] NOT NULL,
[SinhVienID] [bigint] NOT NULL,
[DiemTB_Ky] [decimal] (5, 1) NULL,
[DiemTB_Ky_HB] [decimal] (5, 1) NULL,
[DiemRL_Ky] [decimal] (5, 1) NULL,
[XepLoai_Ky] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BANGTHONGKE] on [dbo].[BANGTHONGKE]'
GO
ALTER TABLE [dbo].[BANGTHONGKE] ADD CONSTRAINT [PK_BANGTHONGKE] PRIMARY KEY CLUSTERED  ([HocKyID], [SinhVienID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BANGTHONGKE_Insert]'
GO
CREATE PROCEDURE [dbo].[BANGTHONGKE_Insert]
	@HocKyID bigint ,
	@SinhVienID bigint ,
	@DiemTB_Ky decimal(5,1) = null ,
	@DiemTB_Ky_HB decimal(5,1) = null ,
	@DiemRL_Ky decimal(5,1) = null ,
	@XepLoai_Ky nvarchar(20) = null 

AS

INSERT [dbo].[BANGTHONGKE]
(
	[HocKyID],
	[SinhVienID],
	[DiemTB_Ky],
	[DiemTB_Ky_HB],
	[DiemRL_Ky],
	[XepLoai_Ky]

)
VALUES
(
	@HocKyID,
	@SinhVienID,
	@DiemTB_Ky,
	@DiemTB_Ky_HB,
	@DiemRL_Ky,
	@XepLoai_Ky

)


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BANGTHONGKE_Update]'
GO
CREATE PROCEDURE [dbo].[BANGTHONGKE_Update]
	@HocKyID bigint,
	@SinhVienID bigint,
	@DiemTB_Ky decimal(5,1) = null,
	@DiemTB_Ky_HB decimal(5,1) = null,
	@DiemRL_Ky decimal(5,1) = null,
	@XepLoai_Ky nvarchar(20) = null

AS

UPDATE [dbo].[BANGTHONGKE]
SET
	[HocKyID] = @HocKyID,
	[SinhVienID] = @SinhVienID,
	[DiemTB_Ky] = @DiemTB_Ky,
	[DiemTB_Ky_HB] = @DiemTB_Ky_HB,
	[DiemRL_Ky] = @DiemRL_Ky,
	[XepLoai_Ky] = @XepLoai_Ky
 WHERE 
	[HocKyID] = @HocKyID AND 
	[SinhVienID] = @SinhVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BANGTHONGKE_SelectAll]'
GO
CREATE PROCEDURE [dbo].[BANGTHONGKE_SelectAll]
AS

	SELECT 
		[HocKyID], [SinhVienID], [DiemTB_Ky], [DiemTB_Ky_HB], [DiemRL_Ky], [XepLoai_Ky]
	FROM [dbo].[BANGTHONGKE]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BANGTHONGKE_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[BANGTHONGKE_DeleteByPrimaryKey]
	@HocKyID bigint,
	@SinhVienID bigint
AS

DELETE FROM [dbo].[BANGTHONGKE]
 WHERE 
	[HocKyID] = @HocKyID AND 
	[SinhVienID] = @SinhVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DIEMTHI]'
GO
CREATE TABLE [dbo].[DIEMTHI]
(
[DiemChuyenCan] [decimal] (5, 1) NULL,
[DiemChuyenCan_HS] [int] NULL,
[DiemGiuaHP] [decimal] (5, 1) NULL,
[DiemGiuaHP_HS] [int] NULL,
[DiemKTThuongXuyen] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DiemTB_HS10] [decimal] (5, 1) NULL,
[DiemTB_HS4] [decimal] (5, 1) NULL,
[DiemTB_TinhHB] [decimal] (5, 1) NULL,
[DiemThiL1] [decimal] (5, 1) NULL,
[DiemThiL2] [decimal] (5, 1) NULL,
[DiemThiL3] [decimal] (5, 1) NULL,
[SinhVienTCID] [bigint] NOT NULL,
[XepLoai] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_DIEMTHI] on [dbo].[DIEMTHI]'
GO
ALTER TABLE [dbo].[DIEMTHI] ADD CONSTRAINT [PK_DIEMTHI] PRIMARY KEY CLUSTERED  ([SinhVienTCID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DIEMTHI_Insert]'
GO
CREATE PROCEDURE [dbo].[DIEMTHI_Insert]
	@DiemChuyenCan decimal(5,1) = null ,
	@DiemChuyenCan_HS int = null ,
	@DiemGiuaHP decimal(5,1) = null ,
	@DiemGiuaHP_HS int = null ,
	@DiemKTThuongXuyen varchar(100) = null ,
	@DiemTB_HS10 decimal(5,1) = null ,
	@DiemTB_HS4 decimal(5,1) = null ,
	@DiemTB_TinhHB decimal(5,1) = null ,
	@DiemThiL1 decimal(5,1) = null ,
	@DiemThiL2 decimal(5,1) = null ,
	@DiemThiL3 decimal(5,1) = null ,
	@SinhVienTCID bigint ,
	@XepLoai nvarchar(10) = null 

AS

INSERT [dbo].[DIEMTHI]
(
	[DiemChuyenCan],
	[DiemChuyenCan_HS],
	[DiemGiuaHP],
	[DiemGiuaHP_HS],
	[DiemKTThuongXuyen],
	[DiemTB_HS10],
	[DiemTB_HS4],
	[DiemTB_TinhHB],
	[DiemThiL1],
	[DiemThiL2],
	[DiemThiL3],
	[SinhVienTCID],
	[XepLoai]

)
VALUES
(
	@DiemChuyenCan,
	@DiemChuyenCan_HS,
	@DiemGiuaHP,
	@DiemGiuaHP_HS,
	@DiemKTThuongXuyen,
	@DiemTB_HS10,
	@DiemTB_HS4,
	@DiemTB_TinhHB,
	@DiemThiL1,
	@DiemThiL2,
	@DiemThiL3,
	@SinhVienTCID,
	@XepLoai

)


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DIEMTHI_Update]'
GO
CREATE PROCEDURE [dbo].[DIEMTHI_Update]
	@DiemChuyenCan decimal(5,1) = null,
	@DiemChuyenCan_HS int = null,
	@DiemGiuaHP decimal(5,1) = null,
	@DiemGiuaHP_HS int = null,
	@DiemKTThuongXuyen varchar(100) = null,
	@DiemTB_HS10 decimal(5,1) = null,
	@DiemTB_HS4 decimal(5,1) = null,
	@DiemTB_TinhHB decimal(5,1) = null,
	@DiemThiL1 decimal(5,1) = null,
	@DiemThiL2 decimal(5,1) = null,
	@DiemThiL3 decimal(5,1) = null,
	@SinhVienTCID bigint,
	@XepLoai nvarchar(10) = null

AS

UPDATE [dbo].[DIEMTHI]
SET
	[DiemChuyenCan] = @DiemChuyenCan,
	[DiemChuyenCan_HS] = @DiemChuyenCan_HS,
	[DiemGiuaHP] = @DiemGiuaHP,
	[DiemGiuaHP_HS] = @DiemGiuaHP_HS,
	[DiemKTThuongXuyen] = @DiemKTThuongXuyen,
	[DiemTB_HS10] = @DiemTB_HS10,
	[DiemTB_HS4] = @DiemTB_HS4,
	[DiemTB_TinhHB] = @DiemTB_TinhHB,
	[DiemThiL1] = @DiemThiL1,
	[DiemThiL2] = @DiemThiL2,
	[DiemThiL3] = @DiemThiL3,
	[SinhVienTCID] = @SinhVienTCID,
	[XepLoai] = @XepLoai
 WHERE 
	[SinhVienTCID] = @SinhVienTCID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DIEMTHI_SelectAll]'
GO
CREATE PROCEDURE [dbo].[DIEMTHI_SelectAll]
AS

	SELECT 
		[DiemChuyenCan], [DiemChuyenCan_HS], [DiemGiuaHP], [DiemGiuaHP_HS], [DiemKTThuongXuyen], [DiemTB_HS10], [DiemTB_HS4], [DiemTB_TinhHB], [DiemThiL1], [DiemThiL2], [DiemThiL3], [SinhVienTCID], [XepLoai]
	FROM [dbo].[DIEMTHI]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DIEMTHI_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[DIEMTHI_DeleteByPrimaryKey]
	@SinhVienTCID bigint
AS

DELETE FROM [dbo].[DIEMTHI]
 WHERE 
	[SinhVienTCID] = @SinhVienTCID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SINHVIENLOPTINCHI]'
GO
CREATE TABLE [dbo].[SINHVIENLOPTINCHI]
(
[HocKyID] [bigint] NULL,
[LopTCID] [bigint] NULL,
[SinhVienID] [bigint] NULL,
[SinhVienTCID] [bigint] NOT NULL IDENTITY(1, 1)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_SINHVIENLOPTINCHI] on [dbo].[SINHVIENLOPTINCHI]'
GO
ALTER TABLE [dbo].[SINHVIENLOPTINCHI] ADD CONSTRAINT [PK_SINHVIENLOPTINCHI] PRIMARY KEY CLUSTERED  ([SinhVienTCID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MONHOC]'
GO
CREATE TABLE [dbo].[MONHOC]
(
[KhoaID] [bigint] NULL,
[MaMonHoc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MonHocID] [bigint] NOT NULL IDENTITY(1, 1),
[SoTC] [int] NULL,
[SoTiet] [int] NULL,
[TenMonHoc] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_MONHOC] on [dbo].[MONHOC]'
GO
ALTER TABLE [dbo].[MONHOC] ADD CONSTRAINT [PK_MONHOC] PRIMARY KEY CLUSTERED  ([MonHocID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LOPTINCHI]'
GO
CREATE TABLE [dbo].[LOPTINCHI]
(
[LopTCID] [bigint] NOT NULL IDENTITY(1, 1),
[MonHocID] [bigint] NULL,
[MaLop] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenLopTC] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_LOPTINCHI] on [dbo].[LOPTINCHI]'
GO
ALTER TABLE [dbo].[LOPTINCHI] ADD CONSTRAINT [PK_LOPTINCHI] PRIMARY KEY CLUSTERED  ([LopTCID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[View_DiemThiTheoKyAndMon]'
GO
CREATE VIEW [dbo].[View_DiemThiTheoKyAndMon]
AS
SELECT     dbo.SINHVIENLOPTINCHI.SinhVienID, dbo.DIEMTHI.SinhVienTCID, dbo.DIEMTHI.DiemTB_HS10, dbo.DIEMTHI.DiemTB_TinhHB, dbo.SINHVIENLOPTINCHI.HocKyID, 
                      dbo.LOPTINCHI.MonHocID, dbo.MONHOC.SoTC, dbo.DIEMTHI.DiemTB_HS4, dbo.DIEMTHI.DiemChuyenCan, dbo.DIEMTHI.DiemGiuaHP, 
                      dbo.DIEMTHI.DiemKTThuongXuyen, dbo.DIEMTHI.DiemThiL1, dbo.DIEMTHI.DiemThiL2, dbo.DIEMTHI.DiemThiL3, dbo.DIEMTHI.XepLoai, dbo.LOPTINCHI.TenLopTC, 
                      dbo.LOPTINCHI.MaLop, dbo.MONHOC.SoTiet, dbo.MONHOC.TenMonHoc, dbo.MONHOC.MaMonHoc, dbo.SINHVIENLOPTINCHI.LopTCID, dbo.MONHOC.KhoaID
FROM         dbo.DIEMTHI LEFT OUTER JOIN
                      dbo.SINHVIENLOPTINCHI ON dbo.DIEMTHI.SinhVienTCID = dbo.SINHVIENLOPTINCHI.SinhVienTCID LEFT OUTER JOIN
                      dbo.LOPTINCHI ON dbo.SINHVIENLOPTINCHI.LopTCID = dbo.LOPTINCHI.LopTCID LEFT OUTER JOIN
                      dbo.MONHOC ON dbo.LOPTINCHI.MonHocID = dbo.MONHOC.MonHocID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SINHVIEN]'
GO
CREATE TABLE [dbo].[SINHVIEN]
(
[GioiTinh] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoTenBo] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoTenMe] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaSV] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgaySinh] [datetime] NULL,
[NgheBo] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgheMe] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[KhoaID] [bigint] NULL,
[LopCoDinhID] [bigint] NULL,
[SinhVienID] [bigint] NOT NULL IDENTITY(1, 1),
[TenSV] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiSinh] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoKhauThuongTru] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiOHienNay] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoDienThoai] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_SINHVIEN] on [dbo].[SINHVIEN]'
GO
ALTER TABLE [dbo].[SINHVIEN] ADD CONSTRAINT [PK_SINHVIEN] PRIMARY KEY CLUSTERED  ([SinhVienID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LOPCODINH]'
GO
CREATE TABLE [dbo].[LOPCODINH]
(
[KhoaID] [bigint] NULL,
[LopID] [bigint] NOT NULL IDENTITY(1, 1),
[MaLopCD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenLopCN] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_LOPCODINH] on [dbo].[LOPCODINH]'
GO
ALTER TABLE [dbo].[LOPCODINH] ADD CONSTRAINT [PK_LOPCODINH] PRIMARY KEY CLUSTERED  ([LopID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KHOA]'
GO
CREATE TABLE [dbo].[KHOA]
(
[KhoaID] [bigint] NOT NULL IDENTITY(1, 1),
[MaKhoa] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenKhoa] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_KHOA] on [dbo].[KHOA]'
GO
ALTER TABLE [dbo].[KHOA] ADD CONSTRAINT [PK_KHOA] PRIMARY KEY CLUSTERED  ([KhoaID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[View_SinhVienFull]'
GO
CREATE VIEW [dbo].[View_SinhVienFull]
AS
SELECT     dbo.SINHVIEN.SinhVienID, dbo.SINHVIEN.MaSV, dbo.SINHVIEN.TenSV, dbo.SINHVIEN.NgaySinh, dbo.SINHVIEN.NoiSinh, dbo.SINHVIEN.HoKhauThuongTru, 
                      dbo.SINHVIEN.NoiOHienNay, dbo.SINHVIEN.Email, dbo.SINHVIEN.SoDienThoai, dbo.SINHVIEN.KhoaID, dbo.SINHVIEN.LopCoDinhID, dbo.SINHVIEN.GioiTinh, 
                      dbo.KHOA.MaKhoa, dbo.KHOA.TenKhoa, dbo.LOPCODINH.MaLopCD, dbo.LOPCODINH.TenLopCN
FROM         dbo.SINHVIEN LEFT OUTER JOIN
                      dbo.LOPCODINH ON dbo.SINHVIEN.LopCoDinhID = dbo.LOPCODINH.LopID LEFT OUTER JOIN
                      dbo.KHOA ON dbo.LOPCODINH.KhoaID = dbo.KHOA.KhoaID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HOCKY]'
GO
CREATE TABLE [dbo].[HOCKY]
(
[HocKyID] [bigint] NOT NULL IDENTITY(1, 1),
[MaHocKy] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamHoc] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenHocKy] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_HOCKY] on [dbo].[HOCKY]'
GO
ALTER TABLE [dbo].[HOCKY] ADD CONSTRAINT [PK_HOCKY] PRIMARY KEY CLUSTERED  ([HocKyID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[View_DiemThiFull]'
GO
CREATE VIEW [dbo].[View_DiemThiFull]
AS
SELECT     dbo.View_SinhVienFull.MaSV, dbo.View_SinhVienFull.TenSV, dbo.View_SinhVienFull.NgaySinh, dbo.HOCKY.TenHocKy, 
                      dbo.View_DiemThiTheoKyAndMon.DiemKTThuongXuyen, dbo.View_DiemThiTheoKyAndMon.DiemChuyenCan, dbo.View_DiemThiTheoKyAndMon.DiemGiuaHP, 
                      dbo.View_DiemThiTheoKyAndMon.DiemThiL1, dbo.View_DiemThiTheoKyAndMon.DiemThiL2, dbo.View_DiemThiTheoKyAndMon.DiemThiL3, 
                      dbo.View_DiemThiTheoKyAndMon.SoTC, dbo.View_DiemThiTheoKyAndMon.DiemTB_HS10, dbo.View_DiemThiTheoKyAndMon.DiemTB_TinhHB, 
                      dbo.View_DiemThiTheoKyAndMon.DiemTB_HS4, dbo.View_DiemThiTheoKyAndMon.XepLoai, dbo.View_DiemThiTheoKyAndMon.HocKyID, 
                      dbo.View_DiemThiTheoKyAndMon.MonHocID, dbo.View_DiemThiTheoKyAndMon.SinhVienID, dbo.View_DiemThiTheoKyAndMon.TenLopTC, 
                      dbo.View_DiemThiTheoKyAndMon.MaLop, dbo.View_DiemThiTheoKyAndMon.SoTiet, dbo.View_DiemThiTheoKyAndMon.TenMonHoc, 
                      dbo.View_DiemThiTheoKyAndMon.MaMonHoc, dbo.View_DiemThiTheoKyAndMon.LopTCID, dbo.View_DiemThiTheoKyAndMon.KhoaID, dbo.KHOA.MaKhoa, 
                      dbo.KHOA.TenKhoa
FROM         dbo.View_DiemThiTheoKyAndMon LEFT OUTER JOIN
                      dbo.View_SinhVienFull ON dbo.View_DiemThiTheoKyAndMon.SinhVienID = dbo.View_SinhVienFull.SinhVienID LEFT OUTER JOIN
                      dbo.HOCKY ON dbo.View_DiemThiTheoKyAndMon.HocKyID = dbo.HOCKY.HocKyID LEFT OUTER JOIN
                      dbo.KHOA ON dbo.View_DiemThiTheoKyAndMon.KhoaID = dbo.KHOA.KhoaID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[View_DiemThiFull_SelectAll]'
GO
CREATE PROCEDURE [dbo].[View_DiemThiFull_SelectAll]
AS

	SELECT 
		[MaSV], [TenSV], [NgaySinh], [TenHocKy], [DiemKTThuongXuyen], [DiemChuyenCan], [DiemGiuaHP], [DiemThiL1], [DiemThiL2], [DiemThiL3], [SoTC], [DiemTB_HS10], [DiemTB_TinhHB], [DiemTB_HS4], [XepLoai], [HocKyID], [MonHocID], [SinhVienID], [TenLopTC], [MaLop], [SoTiet], [TenMonHoc], [MaMonHoc], [LopTCID], [KhoaID], [MaKhoa], [TenKhoa]
	FROM [dbo].[View_DiemThiFull]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[View_DiemThiTheoKyAndMon_SelectAll]'
GO
CREATE PROCEDURE [dbo].[View_DiemThiTheoKyAndMon_SelectAll]
AS

	SELECT 
		[SinhVienID], [SinhVienTCID], [DiemTB_HS10], [DiemTB_TinhHB], [HocKyID], [MonHocID], [SoTC], [DiemTB_HS4], [DiemChuyenCan], [DiemGiuaHP], [DiemKTThuongXuyen], [DiemThiL1], [DiemThiL2], [DiemThiL3], [XepLoai], [TenLopTC], [MaLop], [SoTiet], [TenMonHoc], [MaMonHoc], [LopTCID], [KhoaID]
	FROM [dbo].[View_DiemThiTheoKyAndMon]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[View_SinhVienFull_SelectAll]'
GO
CREATE PROCEDURE [dbo].[View_SinhVienFull_SelectAll]
AS

	SELECT 
		[SinhVienID], [MaSV], [TenSV], [NgaySinh], [NoiSinh], [HoKhauThuongTru], [NoiOHienNay], [Email], [SoDienThoai], [KhoaID], [LopCoDinhID], [GioiTinh], [MaKhoa], [TenKhoa], [MaLopCD], [TenLopCN]
	FROM [dbo].[View_SinhVienFull]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HOCKY_Insert]'
GO
CREATE PROCEDURE [dbo].[HOCKY_Insert]
	@HocKyID bigint output,
	@MaHocKy nvarchar(50) = null ,
	@NamHoc nvarchar(200) = null ,
	@TenHocKy nvarchar(200) = null 

AS

INSERT [dbo].[HOCKY]
(
	[MaHocKy],
	[NamHoc],
	[TenHocKy]

)
VALUES
(
	@MaHocKy,
	@NamHoc,
	@TenHocKy

)
	SELECT @HocKyID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HOCKY_Update]'
GO
CREATE PROCEDURE [dbo].[HOCKY_Update]
	@HocKyID bigint,
	@MaHocKy nvarchar(50) = null,
	@NamHoc nvarchar(200) = null,
	@TenHocKy nvarchar(200) = null

AS

UPDATE [dbo].[HOCKY]
SET
	[MaHocKy] = @MaHocKy,
	[NamHoc] = @NamHoc,
	[TenHocKy] = @TenHocKy
 WHERE 
	[HocKyID] = @HocKyID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HOCKY_SelectAll]'
GO
CREATE PROCEDURE [dbo].[HOCKY_SelectAll]
AS

	SELECT 
		[HocKyID], [MaHocKy], [NamHoc], [TenHocKy]
	FROM [dbo].[HOCKY]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[HOCKY_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[HOCKY_DeleteByPrimaryKey]
	@HocKyID bigint
AS

DELETE FROM [dbo].[HOCKY]
 WHERE 
	[HocKyID] = @HocKyID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KEHOACHGIANGDAY]'
GO
CREATE TABLE [dbo].[KEHOACHGIANGDAY]
(
[GiaoVienID] [bigint] NOT NULL,
[HocKyID] [bigint] NOT NULL,
[LopTCID] [bigint] NOT NULL,
[NgayThi] [datetime] NULL,
[PhongHoc] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_KEHOACHGIANGDAY] on [dbo].[KEHOACHGIANGDAY]'
GO
ALTER TABLE [dbo].[KEHOACHGIANGDAY] ADD CONSTRAINT [PK_KEHOACHGIANGDAY] PRIMARY KEY CLUSTERED  ([GiaoVienID], [HocKyID], [LopTCID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KEHOACHGIANGDAY_Insert]'
GO
CREATE PROCEDURE [dbo].[KEHOACHGIANGDAY_Insert]
	@GiaoVienID bigint ,
	@HocKyID bigint ,
	@LopTCID bigint ,
	@NgayThi datetime = null ,
	@PhongHoc nvarchar(200) = null 

AS

INSERT [dbo].[KEHOACHGIANGDAY]
(
	[GiaoVienID],
	[HocKyID],
	[LopTCID],
	[NgayThi],
	[PhongHoc]

)
VALUES
(
	@GiaoVienID,
	@HocKyID,
	@LopTCID,
	@NgayThi,
	@PhongHoc

)


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KEHOACHGIANGDAY_Update]'
GO
CREATE PROCEDURE [dbo].[KEHOACHGIANGDAY_Update]
	@GiaoVienID bigint,
	@HocKyID bigint,
	@LopTCID bigint,
	@NgayThi datetime = null,
	@PhongHoc nvarchar(200) = null

AS

UPDATE [dbo].[KEHOACHGIANGDAY]
SET
	[GiaoVienID] = @GiaoVienID,
	[HocKyID] = @HocKyID,
	[LopTCID] = @LopTCID,
	[NgayThi] = @NgayThi,
	[PhongHoc] = @PhongHoc
 WHERE 
	[GiaoVienID] = @GiaoVienID AND 
	[HocKyID] = @HocKyID AND 
	[LopTCID] = @LopTCID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KEHOACHGIANGDAY_SelectAll]'
GO
CREATE PROCEDURE [dbo].[KEHOACHGIANGDAY_SelectAll]
AS

	SELECT 
		[GiaoVienID], [HocKyID], [LopTCID], [NgayThi], [PhongHoc]
	FROM [dbo].[KEHOACHGIANGDAY]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KEHOACHGIANGDAY_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[KEHOACHGIANGDAY_DeleteByPrimaryKey]
	@GiaoVienID bigint,
	@HocKyID bigint,
	@LopTCID bigint
AS

DELETE FROM [dbo].[KEHOACHGIANGDAY]
 WHERE 
	[GiaoVienID] = @GiaoVienID AND 
	[HocKyID] = @HocKyID AND 
	[LopTCID] = @LopTCID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KHOA_Insert]'
GO
CREATE PROCEDURE [dbo].[KHOA_Insert]
	@KhoaID bigint output,
	@MaKhoa varchar(50) = null ,
	@TenKhoa nvarchar(200) = null 

AS

INSERT [dbo].[KHOA]
(
	[MaKhoa],
	[TenKhoa]

)
VALUES
(
	@MaKhoa,
	@TenKhoa

)
	SELECT @KhoaID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KHOA_Update]'
GO
CREATE PROCEDURE [dbo].[KHOA_Update]
	@KhoaID bigint,
	@MaKhoa varchar(50) = null,
	@TenKhoa nvarchar(200) = null

AS

UPDATE [dbo].[KHOA]
SET
	[MaKhoa] = @MaKhoa,
	[TenKhoa] = @TenKhoa
 WHERE 
	[KhoaID] = @KhoaID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KHOA_SelectAll]'
GO
CREATE PROCEDURE [dbo].[KHOA_SelectAll]
AS

	SELECT 
		[KhoaID], [MaKhoa], [TenKhoa]
	FROM [dbo].[KHOA]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[KHOA_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[KHOA_DeleteByPrimaryKey]
	@KhoaID bigint
AS

DELETE FROM [dbo].[KHOA]
 WHERE 
	[KhoaID] = @KhoaID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LOPCODINH_Insert]'
GO
CREATE PROCEDURE [dbo].[LOPCODINH_Insert]
	@KhoaID bigint = null ,
	@LopID bigint output,
	@MaLopCD varchar(50) = null ,
	@TenLopCN nvarchar(200) = null 

AS

INSERT [dbo].[LOPCODINH]
(
	[KhoaID],
	[MaLopCD],
	[TenLopCN]

)
VALUES
(
	@KhoaID,
	@MaLopCD,
	@TenLopCN

)
	SELECT @LopID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LOPCODINH_Update]'
GO
CREATE PROCEDURE [dbo].[LOPCODINH_Update]
	@KhoaID bigint = null,
	@LopID bigint,
	@MaLopCD varchar(50) = null,
	@TenLopCN nvarchar(200) = null

AS

UPDATE [dbo].[LOPCODINH]
SET
	[KhoaID] = @KhoaID,
	[MaLopCD] = @MaLopCD,
	[TenLopCN] = @TenLopCN
 WHERE 
	[LopID] = @LopID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LOPCODINH_SelectAll]'
GO
CREATE PROCEDURE [dbo].[LOPCODINH_SelectAll]
AS

	SELECT 
		[KhoaID], [LopID], [MaLopCD], [TenLopCN]
	FROM [dbo].[LOPCODINH]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LOPCODINH_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[LOPCODINH_DeleteByPrimaryKey]
	@LopID bigint
AS

DELETE FROM [dbo].[LOPCODINH]
 WHERE 
	[LopID] = @LopID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MONHOC_Insert]'
GO
CREATE PROCEDURE [dbo].[MONHOC_Insert]
	@KhoaID bigint = null ,
	@MaMonHoc varchar(50) = null ,
	@MonHocID bigint output,
	@SoTC int = null ,
	@SoTiet int = null ,
	@TenMonHoc nvarchar(200) = null 

AS

INSERT [dbo].[MONHOC]
(
	[KhoaID],
	[MaMonHoc],
	[SoTC],
	[SoTiet],
	[TenMonHoc]

)
VALUES
(
	@KhoaID,
	@MaMonHoc,
	@SoTC,
	@SoTiet,
	@TenMonHoc

)
	SELECT @MonHocID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MONHOC_Update]'
GO
CREATE PROCEDURE [dbo].[MONHOC_Update]
	@KhoaID bigint = null,
	@MaMonHoc varchar(50) = null,
	@MonHocID bigint,
	@SoTC int = null,
	@SoTiet int = null,
	@TenMonHoc nvarchar(200) = null

AS

UPDATE [dbo].[MONHOC]
SET
	[KhoaID] = @KhoaID,
	[MaMonHoc] = @MaMonHoc,
	[SoTC] = @SoTC,
	[SoTiet] = @SoTiet,
	[TenMonHoc] = @TenMonHoc
 WHERE 
	[MonHocID] = @MonHocID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MONHOC_SelectAll]'
GO
CREATE PROCEDURE [dbo].[MONHOC_SelectAll]
AS

	SELECT 
		[KhoaID], [MaMonHoc], [MonHocID], [SoTC], [SoTiet], [TenMonHoc]
	FROM [dbo].[MONHOC]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MONHOC_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[MONHOC_DeleteByPrimaryKey]
	@MonHocID bigint
AS

DELETE FROM [dbo].[MONHOC]
 WHERE 
	[MonHocID] = @MonHocID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NGUOIDUNG]'
GO
CREATE TABLE [dbo].[NGUOIDUNG]
(
[CountFail] [int] NULL,
[LastedLogin] [datetime] NULL,
[NguoiDungID] [bigint] NOT NULL IDENTITY(1, 1),
[Password] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_NGUOIDUNG] on [dbo].[NGUOIDUNG]'
GO
ALTER TABLE [dbo].[NGUOIDUNG] ADD CONSTRAINT [PK_NGUOIDUNG] PRIMARY KEY CLUSTERED  ([NguoiDungID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NGUOIDUNG_Insert]'
GO
CREATE PROCEDURE [dbo].[NGUOIDUNG_Insert]
	@CountFail int = null ,
	@LastedLogin datetime = null ,
	@NguoiDungID bigint output,
	@Password nvarchar(200) = null ,
	@UserName nvarchar(50) = null 

AS

INSERT [dbo].[NGUOIDUNG]
(
	[CountFail],
	[LastedLogin],
	[Password],
	[UserName]

)
VALUES
(
	@CountFail,
	@LastedLogin,
	@Password,
	@UserName

)
	SELECT @NguoiDungID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NGUOIDUNG_Update]'
GO
CREATE PROCEDURE [dbo].[NGUOIDUNG_Update]
	@CountFail int = null,
	@LastedLogin datetime = null,
	@NguoiDungID bigint,
	@Password nvarchar(200) = null,
	@UserName nvarchar(50) = null

AS

UPDATE [dbo].[NGUOIDUNG]
SET
	[CountFail] = @CountFail,
	[LastedLogin] = @LastedLogin,
	[Password] = @Password,
	[UserName] = @UserName
 WHERE 
	[NguoiDungID] = @NguoiDungID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NGUOIDUNG_SelectAll]'
GO
CREATE PROCEDURE [dbo].[NGUOIDUNG_SelectAll]
AS

	SELECT 
		[CountFail], [LastedLogin], [NguoiDungID], [Password], [UserName]
	FROM [dbo].[NGUOIDUNG]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[NGUOIDUNG_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[NGUOIDUNG_DeleteByPrimaryKey]
	@NguoiDungID bigint
AS

DELETE FROM [dbo].[NGUOIDUNG]
 WHERE 
	[NguoiDungID] = @NguoiDungID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SINHVIENLOPTINCHI_Insert]'
GO
CREATE PROCEDURE [dbo].[SINHVIENLOPTINCHI_Insert]
	@HocKyID bigint = null ,
	@LopTCID bigint = null ,
	@SinhVienID bigint = null ,
	@SinhVienTCID bigint output

AS

INSERT [dbo].[SINHVIENLOPTINCHI]
(
	[HocKyID],
	[LopTCID],
	[SinhVienID]

)
VALUES
(
	@HocKyID,
	@LopTCID,
	@SinhVienID

)
	SELECT @SinhVienTCID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SINHVIENLOPTINCHI_Update]'
GO
CREATE PROCEDURE [dbo].[SINHVIENLOPTINCHI_Update]
	@HocKyID bigint = null,
	@LopTCID bigint = null,
	@SinhVienID bigint = null,
	@SinhVienTCID bigint

AS

UPDATE [dbo].[SINHVIENLOPTINCHI]
SET
	[HocKyID] = @HocKyID,
	[LopTCID] = @LopTCID,
	[SinhVienID] = @SinhVienID
 WHERE 
	[SinhVienTCID] = @SinhVienTCID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SINHVIENLOPTINCHI_SelectAll]'
GO
CREATE PROCEDURE [dbo].[SINHVIENLOPTINCHI_SelectAll]
AS

	SELECT 
		[HocKyID], [LopTCID], [SinhVienID], [SinhVienTCID]
	FROM [dbo].[SINHVIENLOPTINCHI]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SINHVIENLOPTINCHI_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[SINHVIENLOPTINCHI_DeleteByPrimaryKey]
	@SinhVienTCID bigint
AS

DELETE FROM [dbo].[SINHVIENLOPTINCHI]
 WHERE 
	[SinhVienTCID] = @SinhVienTCID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SINHVIEN_Insert]'
GO
CREATE PROCEDURE [dbo].[SINHVIEN_Insert]
	@GioiTinh nvarchar(10) = null ,
	@HoTenBo nvarchar(200) = null ,
	@HoTenMe nvarchar(200) = null ,
	@MaSV nvarchar(50) = null ,
	@NgaySinh datetime = null ,
	@NgheBo nvarchar(200) = null ,
	@NgheMe nvarchar(200) = null ,
	@KhoaID bigint = null ,
	@LopCoDinhID bigint = null ,
	@SinhVienID bigint output,
	@TenSV nvarchar(200) = null ,
	@NoiSinh nvarchar(400) = null ,
	@HoKhauThuongTru nvarchar(400) = null ,
	@NoiOHienNay nvarchar(400) = null ,
	@Email nvarchar(200) = null ,
	@SoDienThoai nvarchar(50) = null 

AS

INSERT [dbo].[SINHVIEN]
(
	[GioiTinh],
	[HoTenBo],
	[HoTenMe],
	[MaSV],
	[NgaySinh],
	[NgheBo],
	[NgheMe],
	[KhoaID],
	[LopCoDinhID],
	[TenSV],
	[NoiSinh],
	[HoKhauThuongTru],
	[NoiOHienNay],
	[Email],
	[SoDienThoai]

)
VALUES
(
	@GioiTinh,
	@HoTenBo,
	@HoTenMe,
	@MaSV,
	@NgaySinh,
	@NgheBo,
	@NgheMe,
	@KhoaID,
	@LopCoDinhID,
	@TenSV,
	@NoiSinh,
	@HoKhauThuongTru,
	@NoiOHienNay,
	@Email,
	@SoDienThoai

)
	SELECT @SinhVienID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SINHVIEN_Update]'
GO
CREATE PROCEDURE [dbo].[SINHVIEN_Update]
	@GioiTinh nvarchar(10) = null,
	@HoTenBo nvarchar(200) = null,
	@HoTenMe nvarchar(200) = null,
	@MaSV nvarchar(50) = null,
	@NgaySinh datetime = null,
	@NgheBo nvarchar(200) = null,
	@NgheMe nvarchar(200) = null,
	@KhoaID bigint = null,
	@LopCoDinhID bigint = null,
	@SinhVienID bigint,
	@TenSV nvarchar(200) = null,
	@NoiSinh nvarchar(400) = null,
	@HoKhauThuongTru nvarchar(400) = null,
	@NoiOHienNay nvarchar(400) = null,
	@Email nvarchar(200) = null,
	@SoDienThoai nvarchar(50) = null

AS

UPDATE [dbo].[SINHVIEN]
SET
	[GioiTinh] = @GioiTinh,
	[HoTenBo] = @HoTenBo,
	[HoTenMe] = @HoTenMe,
	[MaSV] = @MaSV,
	[NgaySinh] = @NgaySinh,
	[NgheBo] = @NgheBo,
	[NgheMe] = @NgheMe,
	[KhoaID] = @KhoaID,
	[LopCoDinhID] = @LopCoDinhID,
	[TenSV] = @TenSV,
	[NoiSinh] = @NoiSinh,
	[HoKhauThuongTru] = @HoKhauThuongTru,
	[NoiOHienNay] = @NoiOHienNay,
	[Email] = @Email,
	[SoDienThoai] = @SoDienThoai
 WHERE 
	[SinhVienID] = @SinhVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SINHVIEN_SelectAll]'
GO
CREATE PROCEDURE [dbo].[SINHVIEN_SelectAll]
AS

	SELECT 
		[GioiTinh], [HoTenBo], [HoTenMe], [MaSV], [NgaySinh], [NgheBo], [NgheMe], [KhoaID], [LopCoDinhID], [SinhVienID], [TenSV], [NoiSinh], [HoKhauThuongTru], [NoiOHienNay], [Email], [SoDienThoai]
	FROM [dbo].[SINHVIEN]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SINHVIEN_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[SINHVIEN_DeleteByPrimaryKey]
	@SinhVienID bigint
AS

DELETE FROM [dbo].[SINHVIEN]
 WHERE 
	[SinhVienID] = @SinhVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GIAOVIEN]'
GO
CREATE TABLE [dbo].[GIAOVIEN]
(
[GiaoVienID] [bigint] NOT NULL IDENTITY(1, 1),
[HocHam] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HocVi] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaGV] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NamSinh] [datetime] NULL,
[TenGV] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_GIAOVIEN] on [dbo].[GIAOVIEN]'
GO
ALTER TABLE [dbo].[GIAOVIEN] ADD CONSTRAINT [PK_GIAOVIEN] PRIMARY KEY CLUSTERED  ([GiaoVienID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GIAOVIEN_Insert]'
GO
CREATE PROCEDURE [dbo].[GIAOVIEN_Insert]
	@GiaoVienID bigint output,
	@HocHam nvarchar(200) = null ,
	@HocVi nvarchar(200) = null ,
	@MaGV nvarchar(50) = null ,
	@NamSinh datetime = null ,
	@TenGV nvarchar(200) = null 

AS

INSERT [dbo].[GIAOVIEN]
(
	[HocHam],
	[HocVi],
	[MaGV],
	[NamSinh],
	[TenGV]

)
VALUES
(
	@HocHam,
	@HocVi,
	@MaGV,
	@NamSinh,
	@TenGV

)
	SELECT @GiaoVienID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GIAOVIEN_Update]'
GO
CREATE PROCEDURE [dbo].[GIAOVIEN_Update]
	@GiaoVienID bigint,
	@HocHam nvarchar(200) = null,
	@HocVi nvarchar(200) = null,
	@MaGV nvarchar(50) = null,
	@NamSinh datetime = null,
	@TenGV nvarchar(200) = null

AS

UPDATE [dbo].[GIAOVIEN]
SET
	[HocHam] = @HocHam,
	[HocVi] = @HocVi,
	[MaGV] = @MaGV,
	[NamSinh] = @NamSinh,
	[TenGV] = @TenGV
 WHERE 
	[GiaoVienID] = @GiaoVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GIAOVIEN_SelectAll]'
GO
CREATE PROCEDURE [dbo].[GIAOVIEN_SelectAll]
AS

	SELECT 
		[GiaoVienID], [HocHam], [HocVi], [MaGV], [NamSinh], [TenGV]
	FROM [dbo].[GIAOVIEN]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GIAOVIEN_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[GIAOVIEN_DeleteByPrimaryKey]
	@GiaoVienID bigint
AS

DELETE FROM [dbo].[GIAOVIEN]
 WHERE 
	[GiaoVienID] = @GiaoVienID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LOPTINCHI_Insert]'
GO
CREATE PROCEDURE [dbo].[LOPTINCHI_Insert]
	@LopTCID bigint output,
	@MonHocID bigint = null ,
	@MaLop nvarchar(200) = null ,
	@TenLopTC nvarchar(200) = null 

AS

INSERT [dbo].[LOPTINCHI]
(
	[MonHocID],
	[MaLop],
	[TenLopTC]

)
VALUES
(
	@MonHocID,
	@MaLop,
	@TenLopTC

)
	SELECT @LopTCID=SCOPE_IDENTITY();


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LOPTINCHI_Update]'
GO
CREATE PROCEDURE [dbo].[LOPTINCHI_Update]
	@LopTCID bigint,
	@MonHocID bigint = null,
	@MaLop nvarchar(200) = null,
	@TenLopTC nvarchar(200) = null

AS

UPDATE [dbo].[LOPTINCHI]
SET
	[MonHocID] = @MonHocID,
	[MaLop] = @MaLop,
	[TenLopTC] = @TenLopTC
 WHERE 
	[LopTCID] = @LopTCID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LOPTINCHI_SelectAll]'
GO
CREATE PROCEDURE [dbo].[LOPTINCHI_SelectAll]
AS

	SELECT 
		[LopTCID], [MonHocID], [MaLop], [TenLopTC]
	FROM [dbo].[LOPTINCHI]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LOPTINCHI_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[LOPTINCHI_DeleteByPrimaryKey]
	@LopTCID bigint
AS

DELETE FROM [dbo].[LOPTINCHI]
 WHERE 
	[LopTCID] = @LopTCID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BANGDIEMCANHAN_DeleteByPrimaryKey]'
GO
CREATE PROCEDURE [dbo].[BANGDIEMCANHAN_DeleteByPrimaryKey]
	@MaSV varchar(50)
AS

DELETE FROM [dbo].[BANGDIEMCANHAN]
 WHERE 
	[MaSV] = @MaSV

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BANGDIEMCANHAN_Insert]'
GO
CREATE PROCEDURE [dbo].[BANGDIEMCANHAN_Insert]
	@MaKhoa varchar(50) = null ,
	@MaLop varchar(50) = null ,
	@MaMon varchar(50) = null ,
	@MaSV varchar(50) 

AS

INSERT [dbo].[BANGDIEMCANHAN]
(
	[MaKhoa],
	[MaLop],
	[MaMon],
	[MaSV]

)
VALUES
(
	@MaKhoa,
	@MaLop,
	@MaMon,
	@MaSV

)


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BANGDIEMCANHAN_SelectAll]'
GO
CREATE PROCEDURE [dbo].[BANGDIEMCANHAN_SelectAll]
AS

	SELECT 
		[MaKhoa], [MaLop], [MaMon], [MaSV]
	FROM [dbo].[BANGDIEMCANHAN]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BANGDIEMCANHAN_Update]'
GO
CREATE PROCEDURE [dbo].[BANGDIEMCANHAN_Update]
	@MaKhoa varchar(50) = null,
	@MaLop varchar(50) = null,
	@MaMon varchar(50) = null,
	@MaSV varchar(50)

AS

UPDATE [dbo].[BANGDIEMCANHAN]
SET
	[MaKhoa] = @MaKhoa,
	[MaLop] = @MaLop,
	[MaMon] = @MaMon,
	[MaSV] = @MaSV
 WHERE 
	[MaSV] = @MaSV

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating extended properties'
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "View_DiemThiTheoKyAndMon"
            Begin Extent = 
               Top = 1
               Left = 188
               Bottom = 196
               Right = 369
            End
            DisplayFlags = 280
            TopColumn = 13
         End
         Begin Table = "View_SinhVienFull"
            Begin Extent = 
               Top = 15
               Left = 525
               Bottom = 123
               Right = 694
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "HOCKY"
            Begin Extent = 
               Top = 80
               Left = 6
               Bottom = 188
               Right = 157
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "KHOA"
            Begin Extent = 
               Top = 126
               Left = 407
               Bottom = 219
               Right = 558
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append =', 'SCHEMA', N'dbo', 'VIEW', N'View_DiemThiFull', NULL, NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'View_DiemThiFull', NULL, NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'View_DiemThiFull', NULL, NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DIEMTHI"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 214
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "SINHVIENLOPTINCHI"
            Begin Extent = 
               Top = 6
               Left = 257
               Bottom = 153
               Right = 422
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LOPTINCHI"
            Begin Extent = 
               Top = 6
               Left = 446
               Bottom = 181
               Right = 597
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MONHOC"
            Begin Extent = 
               Top = 6
               Left = 635
               Bottom = 177
               Right = 786
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 21
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
        ', 'SCHEMA', N'dbo', 'VIEW', N'View_DiemThiTheoKyAndMon', NULL, NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N' Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'View_DiemThiTheoKyAndMon', NULL, NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'View_DiemThiTheoKyAndMon', NULL, NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SINHVIEN"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 202
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LOPCODINH"
            Begin Extent = 
               Top = 6
               Left = 245
               Bottom = 114
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "KHOA"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 99
               Right = 585
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'View_SinhVienFull', NULL, NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'View_SinhVienFull', NULL, NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO